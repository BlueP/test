﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AppPic.DTOs
{
    public class EnumValue : System.Attribute
    {
        private string _value;
        public EnumValue(string value)
        {
            _value = value;
        }
        public string Value
        {
            get { return _value; }
        }
    }

    public static class EnumString
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            EnumValue[] attrs = fi.GetCustomAttributes(typeof(EnumValue), false) as EnumValue[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Value;
            }
            return output;
        }
    }

    public static class EnumExtension
    {
        public static string ToDescription(this System.Enum value)
        {
            try
            {
                var attributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return attributes.Length > 0 ? attributes[0].Description : value.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
    }


    public class Location
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class AppLanguage
    {
        public AppLanguageSelect Language { get; set; }
    }

    public enum AppLanguageSelect
    {
        [Description("Englist")]
        Englist = 1,
        [Description("Japanese")]
        Japanese = 2,
    }

}
