﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppPic.DTOs
{
    public class Notification
    {
        public string Device_ID { get; set; }
        public bool IsEnable { get; set; }
    }
}
