﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppPic.DTOs
{
    public class UserToken
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }

    public class ClientToken
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
    }
}
